# container
FROM node:14.15.1-alpine

RUN apk update
RUN apk add openssh
RUN apk add --no-cache lftp
RUN mkdir -p ~/.ssh

CMD [""]
